# Actions

Here, you will find examples of actions with different programs.

- [firewalld](firewalld.md)
- [iptables](iptables.md)
- [nftables](nftables.md)
- [PacketFilter](pf.md)
- [SMS alerting with FreeMobile](sms-free.md)
