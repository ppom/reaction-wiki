# Common Log Format

The common log format is supported by most webservers.

```nginx
<remote_IP_address> - <client_user_name_if_available> [<timestamp>] "<request_method> <request_path> <request_protocol>" <HTTP_status> <content-length> "<request_referrer>" "<request_user_agent>" <number_of_requests_received_since_webserver_started> "<router_name>" "<server_URL>" <request_duration_in_ms>ms
```

Examples of reaction log regexes with a webserver that uses the CLF:

```jsonnet
@'^<ip> .* "POST /auth/login HTTP/..." 401 [0-9]+ "https://domain.name/.*'
// ^        ^    ^                     ^                   ^      
// IP  Method    Path        Status Code                   Domain 
```

```jsonnet
@'^<ip> .* "(GET|POST) /login HTTP/..." 401 '
// ^        ^          ^                ^
// IP       Method     Path             Status Code
```
