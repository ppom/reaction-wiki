# Filters

Here, you will find examples of filters for different programs’ logs.

- [AI crawlers (ChatGPT...)](ai-crawlers.md)
- [Dolibarr](dolibarr.md)
- [Directus](directus.md)
- [Nextcloud](nextcloud.md)
- [Nginx](nginx.md)
- [Slskd](slskd.md)
- [SSH](ssh.md)
- [Traefik](traefik.md)
- [Web crawlers](web-crawlers.md)
- [Web servers common log format](webservers-common-log-format.md)
