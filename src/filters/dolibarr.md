# Dolibarr

Configuration for a [Dolibarr](https://www.dolibarr.org/) instance.
Dolibarr is one of the leading open source ERP/CRM web applications.

*As an action, we'll use iptables. See [here](/actions/iptables).*

*As a pattern, we'll use ip. See [here](/patterns#ip).*

Dolibarr "logs" module must be activated !

```jsonnet
{
  streams: {
    // Ban hosts failing to connect to Dolibarr
    dolibarr: {

      cmd: ['tail', '-fn0', '/path/to/dolibarr/documents/dolibarr.log'],

      filters: {
        bad_password: {
          regex: [
            @'NOTICE  <ip> .*Bad password, connexion refused',
          ],
          retry: 3,
          retryperiod: '1h',
          actions: banFor('48h'),
                        },
                },
            },
        },
    }
```
