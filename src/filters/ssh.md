# SSH

Configuration for the [OpenSSH](https://github.com/openssh/openssh-portable) service.

*As an action, we'll use iptables. See [here](/actions/iptables).*

*As a pattern, we'll use ip. See [here](/patterns#ip).*

```jsonnet
{
  streams: {
    // Ban hosts failing to connect via ssh
    ssh: {
      // Use systemd's `journalctl` to tail logs
      cmd: ['journalctl', '-fn0', '-u', 'ssh.service'],
                        // ⚠️ may also be ↑ sshd.service, depends on the distribution
      filters: {
        failedlogin: {
          regex: [
            // Auth fail
            @'authentication failure;.*rhost=<ip>',
            // Client disconnects during authentication
            @'Connection (reset|closed) by (authenticating|invalid) user .* <ip> port',
            @'Connection (reset|closed) by <ip> port',
            // More specific auth fail
            @'Failed password for .* from <ip>',
            //Other auth failures
            @'banner exchange: Connection from <ip> port [0-9]*: invalid format',
            @'Invalid user .* from <ip>',
          ],
          retry: 3,
          retryperiod: '6h',
          actions: banFor('48h'),
        },
      },
    },
  },
}
```
## OpenBSD

```jsonnet
{
  streams: {
    // Ban hosts failing to connect via ssh
    ssh: {
      // Use  `/var/log/authlog` to tail logs
      cmd: ['tail', '-fn0', '/var/log/authlog'],
      filters: {
        failedlogin: {
          regex: [
            // Auth fail
            @'Failed password for invalid user .* from <ip>',
            // Client disconnects during authentication
            @'Disconected from invalid user .* <ip>',
          ],
          retry: 3,
          retryperiod: '6h',
          actions: banFor('48h'),
        },
      },
    },
  },
}
```

Depending on the Linux distributions (or other UNIX systems), your OpenSSH logs may vary.

Check yourself what logs are printed by your SSH server!
