# slskd

Configuration for the [slskd](https://github.com/slskd/slskd/) web service.

slskd doesn't log failed login attempts, so one must read the logs of the reverse proxy
configured in front of slskd.

The HTTP code sent after a failed login is `401`, *Unauthorized*.

The request to authenticate on slskd is a POST on `/api/v0/session`.

*As a pattern, we'll use ip. See [here](/patterns#ip).*

A regex for nginx can look like this:
```jsonnet
@'^<ip> .* slskd.domain "POST /api/v0/session HTTP/..." 401 ',
```
- adjust https://slskd.domain according to your domain.
- if slskd is served on a subpath, say `/slskd`, then adjust to `POST /slskd/auth/login`

Example: 
```jsonnet
{
  streams: {
    nginx: {
      cmd: ['...'], // see ./nginx.md
      filters: {
        slskd: {
          regex: [
            @'^<ip> .* slskd.domain "POST /api/v0/session HTTP/..." 401 ',
          ],
          retry: 3,
          retryperiod: '6h',
          actions: banFor('48h'),
        },
      },
    },
  },
}
```
