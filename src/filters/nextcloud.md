# Nextcloud

Configuration for the [Nextcloud](https://nextcloud.io) web service.

Nextcloud logs failed login attempts, so we will read Nextcloud logs.

We can't use reverse proxy's logs,
because when a user logins,
using a POST on `/login`,
the HTTP status code responded by Nextcloud is always the same:
`303`, *See Other*.
(That means the client has to reload the same page, but using GET).

*As a pattern, we'll use ip. See [here](/patterns#ip).*

See [Nextcloud documentation on logging](https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/logging_configuration.html)
to check where your application logs are.

There are multiple log configurations possible with Nextcloud.
The example covers 2 cases, but there are more! *Feel free to contribute your own if you think it's relevant.*

Example: 
```jsonnet
{
  streams: {
    nextcloud: {

      // with a PHP-FPM worker logging to systemd
      cmd: ['journalctl', '-fn0', '-u', 'phpfpm-nextcloud.service'],

      // when logging to a file
      cmd: ['tail', '-fn0', 'NEXTCLOUD_DIR/data/nextcloud.log'],

      filters: {
        nextcloud: {
          regex: [
            @'"remoteAddr":"<ip>".*"message":"Login failed:',
            @'"remoteAddr":"<ip>".*"message":"Trusted domain error.',
          ],
          retry: 3,
          retryperiod: '1h',
          actions: banFor('3h'),
        },
      },
    },
  },
}
```
