# Summary

[Introduction](index.md)

- [Tutorials](tutorials.md)

# Good practices

- [Security](security.md)
- [JSONnet](jsonnet.md)
- [Stream sources](streams.md)

# Examples

- [Patterns](patterns.md)
- [Filters](filters/index.md)
  - [AI crawlers (ChatGPT...)](filters/ai-crawlers.md)
  - [Dolibarr](filters/dolibarr.md)
  - [Directus](filters/directus.md)
  - [Nextcloud](filters/nextcloud.md)
  - [Nginx](filters/nginx.md)
  - [Slskd](filters/slskd.md)
  - [SSH](filters/ssh.md)
  - [Traefik](filters/traefik.md)
  - [Web crawlers](filters/web-crawlers.md)
  - [Web servers common log format](filters/webservers-common-log-format.md)
- [Actions](actions/index.md)
  - [firewalld](actions/firewalld.md)
  - [iptables](actions/iptables.md)
  - [nftables](actions/nftables.md)
  - [PacketFilter](actions/pf.md)
  - [SMS alerting with FreeMobile](actions/sms-free.md)
- [Configurations](configurations/index.md)
  - [Configs of ppom](configurations/ppom/index.md)
    - [`server.jsonnet`](configurations/ppom/server.jsonnet.md)
    - [`activitywatch.jsonnet`](configurations/ppom/activitywatch.jsonnet.md)
  - [OpenBSD config](configurations/OpenBSD.md)
- [migrate-to-v2](migrate-to-v2.md): migration from reaction v1 to reaction v2
