# Patterns

Patterns are globally defined. They're substitued in regexes.

The regex syntax of `reaction` is documented [here](https://github.com/google/re2/wiki/Syntax).

When a filter performs an action, it replaces the found pattern by the regex.

```jsonnet
{
  patterns: {
    // <ip> is defined here
    ip: {
      regex: '...',
    },
  },

  streams: {
    myservice: {
      cmd: [ 'echo', 'the IP 1.2.3.4 is a bot!' ],
      filters: {
        myfilter: {
          // ip regex will be substitued in this regex at <ip>
          regex: [ '^the IP <ip> is a bot!' ],
          actions: {
            myaction: {
              // when executed, <ip> will be substitued by the IP found by the filter
              cmd: [ '/path/to/ban.sh', '<ip>' ],
              // executes: /path/to/ban.sh 1.2.3.4
            },
          },
        },
      },
    },
  },
}
```

## IP

There are both simple and full versions.

- Simple versions should be faster, but they may also accept malformed IPs, even any hexadecimal content when using IPv6 regexes.
- Full versions mean to be entirely correct (no false positives), adapted from IHateRegex.io ([ipv4](https://ihateregex.io/expr/ip/), [ipv6](https://ihateregex.io/expr/ipv6/)).

### IPv4 only

Simple version:

```jsonnet
{
  patterns: {
    ipv4: {
      regex: @'(([0-9]{1,3}\.){3}[0-9]{1,3})',
      ignore: [
        '127.0.0.1' // do not ban localhost!
        // it can be also advised to avoid banning your Internet Gateway (the router)
      ]
    },
  },
}
```

Full version:

```jsonnet
{
  patterns: {
    ipv4: {
      regex: @'(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}',
      ignore: [
        '127.0.0.1' // do not ban localhost!
        // it can be also advised to avoid banning your Internet Gateway (the router)
      ]
    },
  },
}
```

### IPv6 only

Simple version:

```jsonnet
{
  patterns: {
    ipv6: {
      regex: @'([0-9a-fA-F:]{2,90})',
      ignore: [
        '::1' // do not ban localhost!
        // it can be also advised to avoid banning your Internet Gateway (the router)
      ]
    },
  },
}
```

Full version:

```jsonnet
{
  patterns: {
    ipv6: {
      regex: @'(?:[0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,7}:|(?:[0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,5}(?::[0-9a-fA-F]{1,4}){1,2}|(?:[0-9a-fA-F]{1,4}:){1,4}(?::[0-9a-fA-F]{1,4}){1,3}|(?:[0-9a-fA-F]{1,4}:){1,3}(?::[0-9a-fA-F]{1,4}){1,4}|(?:[0-9a-fA-F]{1,4}:){1,2}(?::[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:(?:(?::[0-9a-fA-F]{1,4}){1,6})|:(?:(?::[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(?::[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(?:ffff(?::0{1,4}){0,1}:){0,1}(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])|(?:[0-9a-fA-F]{1,4}:){1,4}:(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])',
      ignore: [
        '::1' // do not ban localhost!
        // it can be also advised to avoid banning your Internet Gateway (the router)
      ]
    },
  },
}
```

## Both IPv4 and IPv6

Simple version:

```jsonnet
{
  patterns: {
    ip: {
      // reaction regex syntax is defined here: https://github.com/google/re2/wiki/Syntax
      // jsonnet's @'string' is for verbatim strings
      regex: @'(?:(?:[0-9]{1,3}\.){3}[0-9]{1,3})|(?:[0-9a-fA-F:]{2,90})',
      ignore: [
        // do not ban localhost!
        '127.0.0.1',
        '::1',
        // it can be also advised to avoid banning your Internet Gateway (the router)
      ],
    },
  },
}
```

Full version:

```jsonnet
{
  patterns: {
    ip: {
      regex: @'(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}|(?:(?:[0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,7}:|(?:[0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|(?:[0-9a-fA-F]{1,4}:){1,5}(?::[0-9a-fA-F]{1,4}){1,2}|(?:[0-9a-fA-F]{1,4}:){1,4}(?::[0-9a-fA-F]{1,4}){1,3}|(?:[0-9a-fA-F]{1,4}:){1,3}(?::[0-9a-fA-F]{1,4}){1,4}|(?:[0-9a-fA-F]{1,4}:){1,2}(?::[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:(?:(?::[0-9a-fA-F]{1,4}){1,6})|:(?:(?::[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(?::[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(?:ffff(?::0{1,4}){0,1}:){0,1}(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])|(?:[0-9a-fA-F]{1,4}:){1,4}:(?:(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(?:25[0-5]|(?:2[0-4]|1{0,1}[0-9]){0,1}[0-9]))',
      ignore: [
        '127.0.0.1' // do not ban localhost!
        '::1' // do not ban localhost!
        // it can be also advised to avoid banning your Internet Gateway (the router)
      ]
    },
  },
}
```
