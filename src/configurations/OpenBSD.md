# OpenBSD setup

## Building the binary

Binary needs to be compiled for OpenBSD OS.
```sh
git clone <reaction git repo>
cd reaction
GOOS=openbsd make reaction
```


## Service

To use reaction as service on **OpenBSD**, you will need to create a new file on `/etc/rc.d/` which will define your service, such as :

```sh

#!/bin/ksh

daemon="/usr/local/bin/reaction"
daemon_flags="start -c /root/reaction.conf"

. /etc/rc.d/rc.subr

rc_reload=NO
rc_bg=YES

rc_cmd $1

```

Then you need enable it with `rcctl enable reaction`.

## Configuration

Configuration differs from regular linux one as OpenBSD uses PacketFilter instead of iptables/nftables.
Also, OpenBSD configuration doesn't need the additional `ip46tables` binary, and can be fully setup from configuration file.

A configuration exemple is present [here](/actions/pf.md).

## To infinity and beyond

After configuring your instance and setting up your service, you can now enjoy it by running `rcctl start reaction`.

