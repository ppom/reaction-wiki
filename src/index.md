# reaction

A daemon that scans program outputs for repeated patterns, and takes action.

A common usage is to scan ssh and webserver logs, and to ban hosts that cause multiple authentication errors.

## Welcome on reaction's Wiki!

Wiki rendered on <https://reaction.ppom.me>.

For an introduction to reaction, see:
- Its [README](https://framagit.org/ppom/reaction)
- The tutorial, in [French](https://blog.ppom.me/fr-reaction) and in [English](https://blog.ppom.me/en-reaction).
- The example, fully commented configuration file, in [YAML](https://framagit.org/ppom/reaction/-/blob/main/app/example.yml) or in [JSONnet](https://framagit.org/ppom/reaction/-/blob/main/config/example.jsonnet).

This wiki is made of:

- [security](/security.md): good practices to avoid giving arbitrary execution to attackers. A must read!
- [jsonnet](/jsonnet.md): FAQ, help and good practices about JSONnet.
- [streams](/streams.md): good practices about stream sources
- [patterns](/patterns.md): good practices and common defaults about patterns
- [filters](/filters/index.md): discover existing service configurations
- [actions](/actions/index.md): discover existing actions
- [configurations](/configurations/index.md): discover real-world user configurations
- [migrate-to-v2](/migrate-to-v2.md): migration from reaction v1 to reaction v2 (smooth!)

❤️ Please enhance this wiki with your own discoveries! ❤️
