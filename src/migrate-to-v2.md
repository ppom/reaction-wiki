# Reaction migration from v1 to v2

For now reaction v2 is released as `2.0.0-rc2`, which means it's considered beta.

Feel free to try it, if you feel playful!
Error reports are very welcome.

## Configuration

A working v1 configuration should work as well on reaction v2.

There are small syntax differences between the two regex engines:

- [v1](https://github.com/google/re2/wiki/Syntax)
- [v2](https://docs.rs/regex/latest/regex/#syntax)

However, most regexes should be unaffected.

Please report any configuration / regex problem you encounter while upgrading!

## Database

reaction-v1 and reaction-v2 databases are not compatible.

The persisted memory of reaction will be lost.

## CLI

The CLI is the same, but more flexible. Mixing positional arguments and flags is now supported.

## Packaging

Reaction now features man pages and shell completions for bash/fish/zsh!
Documentation for their installation is available in the [release page](https://framagit.org/ppom/reaction/-/releases/v2.0.0-rc2)

The releases contain a debian package and a .tar archive.

The reaction binary is fully static and should work on any Linux distribution.

## Streams

on reaction v1, only the `cmd` stdout was read.
reaction v2 now reads both stdout and stderr.
